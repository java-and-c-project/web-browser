import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

 public class JourneyApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Journey to Destination");

        // Create ChoiceBox for "From" button
        ChoiceBox<String> fromChoiceBox = new ChoiceBox<>();
        fromChoiceBox.getItems().addAll("City A", "City B", "City C");
        fromChoiceBox.setValue("Select From City");

        // Create ChoiceBox for "To" button
        ChoiceBox<String> toChoiceBox = new ChoiceBox<>();
        toChoiceBox.getItems().addAll("City X", "City Y", "City Z");
        toChoiceBox.setValue("Select To City");

        // Create buttons
        Button fromButton = new Button("From");
        Button toButton = new Button("To");

        // Set action for "From" button
        fromButton.setOnAction(e -> {
            String selectedCity = fromChoiceBox.getValue();
            System.out.println("From: " + selectedCity);
        });

        // Set action for "To" button
        toButton.setOnAction(e -> {
            String selectedCity = toChoiceBox.getValue();
            System.out.println("To: " + selectedCity);
        });

        // Create layout
        HBox layout = new HBox(10);
        layout.setPadding(new Insets(10));
        layout.getChildren().addAll(fromButton, fromChoiceBox, toButton, toChoiceBox);

        // Create scene
        Scene scene = new Scene(layout, 400, 100);

        // Set the scene to the stage
        primaryStage.setScene(scene);

        // Show the stage
        primaryStage.show();
    }
}
